{
    inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";

    outputs = { self, nixpkgs }: let
        supportedSystems = [
            "x86_64-linux"
            "i686-linux"
            "x86_64-darwin"
            "aarch64-linux"
            "armv6l-linux"
            "armv7l-linux"
            "aarch64-darwin"
        ];
        forAllSystems = f: nixpkgs.lib.genAttrs supportedSystems (system: f system);   
    in {
        packages = forAllSystems (system:
            nixpkgs.lib.attrsets.filterAttrs
                (name: val: name != "buildFirefoxXpiAddon") # remove buildFirefoxXpiAddon function
                (import ./. { inherit (nixpkgs.legacyPackages.${system}) fetchurl lib stdenv; })
        );
    };
}